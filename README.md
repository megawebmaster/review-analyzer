# Review Analyzer

Simple tool for analysing how team members cooperate during defined time periods based on statistical data from code review comments.

## Authors

Amadeusz Starzykiewicz <megawebmaster@gmail.com>

